﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    public static MenuManager instance;

    public List<GameObject> screens = new List<GameObject>();
    GameObject currentScreen, previousScreen;
    bool calledTransition;

    private void Awake() {
        instance = this;
    }

    public void StartMenuManager() {
        DebugScreenManager.instance.WriteDebug(DebugScreenManager.MessageType.Start, "Menu Manager");
        currentScreen = screens[0];
        currentScreen.SetActive(true);

    }

    private void Update() {
        if (Transitioner.waitingChanges && calledTransition) {
            ChangeScreen();
        }
    }

    public void CallChangeScreen (string newScreen) {
        for (int i = 0; i < screens.Count; i++) {
            if (screens[i].GetComponent<MenuHolster>().screenID == newScreen) {
                Transitioner.instance.CallFadeIn(Color.black, 1f);
                previousScreen = currentScreen;
                currentScreen = screens[i];
                calledTransition = true;
                return;
            } else if (i == screens.Count - 1) { 
                Debug.Log("Next Screen can't be found.");
            }
        }
    }

    void ChangeScreen() {
        previousScreen.SetActive(false);
        currentScreen.SetActive(true);
        calledTransition = false;
        Transitioner.instance.CallFadeOut();
    }
}
