﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InformationHolster : MonoBehaviour {

    public enum InformationType {
        Text,
        SmallImage,
        MediumImage,
        SmallAnimation,
        MediumAnimation,
    }

    public InformationType informationType;

    public void FillInformation (string information) {
        switch (informationType) {
            case InformationType.Text:
                GetComponent<Text>().text = information;
                break;
            case InformationType.SmallImage:
                GetComponent<Image>().sprite = (Sprite)Resources.Load("Art/Small/" + information + ".png");
                break;
            case InformationType.MediumImage:
                GetComponent<Image>().sprite = (Sprite)Resources.Load("Art/Medium/" + information + ".png");
                break;
            case InformationType.SmallAnimation:
                GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Art/Small/" + information + "_S.controller");
                break;
            case InformationType.MediumAnimation:
                GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Art/Small/" + information + "_M.controller");
                break;
        }
    }

    public void Clean () {
        switch (informationType) {
            case InformationType.Text:
                GetComponent<Text>().text = "";
                break;
            case InformationType.SmallImage:
                GetComponent<Image>().sprite = (Sprite)Resources.Load("Art/Small/Loading.png");
                break;
            case InformationType.MediumImage:
                GetComponent<Image>().sprite = (Sprite)Resources.Load("Art/Medium/Loading.png");
                break;
            case InformationType.SmallAnimation:
                GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Art/Animations/Loading_S.controller");
                break;
            case InformationType.MediumAnimation:
                GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Art/Animations/Loading_M.controller");
                break;
        }
    }
}
