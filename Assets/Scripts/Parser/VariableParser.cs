﻿using System;
using System.Collections.Generic;

public static class VariableParser {
    
    public static int[] StringToIntArray(string t) {
        string[] strArr = t.Split('@');
        int[] intArr = new int[strArr.Length];
        for (int i = 0; i < strArr.Length - 1; i++) {
            string temp = strArr[i];
            intArr[i] = int.Parse(temp);
        }
        return intArr;
    }
}
