﻿[System.Serializable]
public class ExpRate {

    int id;
    string name;
    int[] perLvl = new int[101];

    public ExpRate () {
        id = 0;
        name = "";
        CleanLvlArray();
    }

    public ExpRate (int id_, string name_, int[] perLvl_) {
        id = id_;
        name = name_;
        perLvl = perLvl_;
    }

    //Fake Constructor for Parser
    public ExpRate (string id_, string name_, string perLvl_) {
        id = int.Parse(id_);
        name = name_;
        perLvl = VariableParser.StringToIntArray(perLvl_);
        if (GlobalArchive.EntryExists(id_, GlobalArchive.ListType.ExpRate))
            GlobalArchive.ReplaceByID(id, new ExpRate(id, name, perLvl));
        else
            GlobalArchive.expRateList.Add(new ExpRate(id, name, perLvl));
    }

    //Getters & Setters
    public int GetID() {
        return id;
    }
    public void SetID (int newID) {
        id = newID;
    }

    public string GetName() {
        return name;
    }
    public void SetName(string newName) {
        name = newName;
    }

    public int GetLvlExp (int level) {
        if (level < perLvl.Length)
            return perLvl[level];
        return -1;
    }
    public void SetLvlExp(int level, int exp) {
        if (level < perLvl.Length)
            perLvl[level] = exp;
    }

    //Array actions

    public void CleanLvlArray () {
        for (int i = 0; i < perLvl.Length; i++) {
            perLvl[i] = 0;
        }
    }

    public void EqualLvlArrayTo (int[] lvlArray) {
        if (lvlArray.Length != perLvl.Length) {
            for (int i = 0; i < perLvl.Length; i++) {
                perLvl[i] = lvlArray[i];
            }
        }
    }


}
