﻿using System.Collections.Generic;

[System.Serializable]
public class Ability {

    int id;
    string name;
    string description;

    public Ability () {
        id = 0;
        name = "";
        description = "";
    }

    public Ability (int id_, string name_, string description_) {
        id = id_;
        name = name_;
        description = description_;
    }

    //Fake Constructor for Parser
    public Ability (string id_, string name_, string description_) {
        id = int.Parse(id_);
        name = name_;
        description = description_;
        if (GlobalArchive.EntryExists(id_, GlobalArchive.ListType.Ability))
            GlobalArchive.ReplaceByID(id, new Ability(id, name, description));
        else
            GlobalArchive.abilityList.Add(new Ability(id, name, description));
    }

    //Getters & Setters
    public int GetID () {
        return id;
    }
    public void SetID (int newID) {
        id = newID;
    }

    public string GetName () {
        return name;
    }
    public void SetName (string newName) {
        name = newName;
    }

    public string GetDescription () {
        return description;
    }
    public void SetDescription (string newDescription) {
        description = newDescription;
    }

    public List<string> GetInformation() {
        List<string> t = new List<string>();
        t.Add(name);
        t.Add(description);
        return t;
    }
}
