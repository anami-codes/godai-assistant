﻿using System.Linq;
using System.Collections.Generic;
using System;

public static class GlobalArchive {

    public enum ListType {
        Ability,
        SortedAbility,
        Division,
        ExpRate,
        HealthType,
        PokemonType,
    }

    public static float currentVersion = 0.0f;
    public static List<Ability> abilityList = new List<Ability>();
    public static List<Ability> sortedAbilityList = new List<Ability>();
    public static List<Division> divisionList = new List<Division>();
    public static List<ExpRate> expRateList = new List<ExpRate>();
    public static List<HealthType> healthTypeList = new List<HealthType>();
    public static List<PokemonType> pokemonTypeList = new List<PokemonType>();

    public static bool CheckVersion (string className, string version) {
        string temp = version.TrimStart('v');
        float docVersion = float.Parse(temp);
        version = "";
        temp = "";
        switch (className) {
            case "Ability":
                if (abilityList.Count == 0)
                    version = "v0.0";
                else
                    version = abilityList[0].GetName();
                break;
            case "Division":
                if (divisionList.Count == 0)
                    version = "v0.0";
                else
                    version = divisionList[0].GetName();
                break;
            case "ExpRate":
                if (expRateList.Count == 0)
                    version = "v0.0";
               else
                    version = expRateList[0].GetName();
               break;
            case "HealthType":
                if (healthTypeList.Count == 0)
                    version = "v0.0";
                else
                    version = healthTypeList[0].GetName();
                break;
            case "PokemonType":
                if (pokemonTypeList.Count == 0)
                    version = "v0.0";
                else
                    version = pokemonTypeList[0].GetName();
                break;
        }
        temp = version.TrimStart('v');
        float classVersion = float.Parse(temp);
        if (docVersion > currentVersion) currentVersion = docVersion;
        if (docVersion > classVersion) return false;
        return true;
    }

    public static void SortLists () {
        sortedAbilityList = abilityList.OrderBy(o => o.GetName()).ToList();
        sortedAbilityList.Remove(abilityList[0]);
    }

    public static bool EntryExists (string id, ListType listType) {
        switch (listType) {
            case ListType.Ability:
                for (int i = 0; i < abilityList.Count; i++) {
                    if (abilityList[i].GetID() == int.Parse(id))
                        return true;
                }
                break;
            case ListType.Division:
                for (int i = 0; i < divisionList.Count; i++) {
                    if (divisionList[i].GetID() == int.Parse(id))
                        return true;
                }
                break;
            case ListType.ExpRate:
                for (int i = 0; i < expRateList.Count; i++) {
                    if (expRateList[i].GetID() == int.Parse(id))
                        return true;
                }
                break;
            case ListType.HealthType:
                for (int i = 0; i < healthTypeList.Count; i++) {
                    if (healthTypeList[i].GetID() == int.Parse(id))
                        return true;
                }
                break;
            case ListType.PokemonType:
                for (int i = 0; i < pokemonTypeList.Count; i++) {
                    if (pokemonTypeList[i].GetID() == int.Parse(id))
                        return true;
                }
                break;
        }
        return false;
    }

    public static int GetEntryID (string name, ListType listType) {

        switch (listType) {
            case ListType.Ability:
                for (int i = 0; i < abilityList.Count; i++) {
                    if (String.Equals(name, abilityList[i].GetName(), StringComparison.OrdinalIgnoreCase))
                        return i;
                }
                break;
            case ListType.SortedAbility:
                for (int i = 0; i < sortedAbilityList.Count; i++) {
                    if (String.Equals(name, sortedAbilityList[i].GetName(), StringComparison.OrdinalIgnoreCase))
                        return i;
                }
                break;
            case ListType.Division:
                for (int i = 0; i < divisionList.Count; i++) {
                    if (divisionList[i].GetName() == name)
                        return i;
                }
                break;
            case ListType.ExpRate:
                for (int i = 0; i < expRateList.Count; i++) {
                    if (String.Equals(name, expRateList[i].GetName(), StringComparison.OrdinalIgnoreCase))
                        return i;
                }
                break;
            case ListType.HealthType:
                for (int i = 0; i < healthTypeList.Count; i++) {
                    if (String.Equals(name, healthTypeList[i].GetName(), StringComparison.OrdinalIgnoreCase))
                        return i;
                }
                break;
            case ListType.PokemonType:
                for (int i = 0; i < pokemonTypeList.Count; i++) {
                    if (String.Equals(name, pokemonTypeList[i].GetName(), StringComparison.OrdinalIgnoreCase))
                        return i;
                }
                break;
        }
        return -1;
    }

    public static void ReplaceByID(int id, Division newOBJ) {
        for (int i = 0; i < divisionList.Count; i++) {
            if (divisionList[i].GetID() == id) divisionList[i] = newOBJ;
        }
    }

    public static void ReplaceByID(int id, Ability newOBJ) {
        for (int i = 0; i < abilityList.Count; i++)
        {
            if (abilityList[i].GetID() == id) abilityList[i] = newOBJ;
        }
    }

    public static void ReplaceByID(int id, ExpRate newOBJ) {
        for (int i = 0; i < expRateList.Count; i++)
        {
            if (expRateList[i].GetID() == id) expRateList[i] = newOBJ;
        }
    }

    public static void ReplaceByID(int id, HealthType newOBJ) {
        for (int i = 0; i < healthTypeList.Count; i++)
        {
            if (healthTypeList[i].GetID() == id) healthTypeList[i] = newOBJ;
        }
    }

    public static void ReplaceByID(int id, PokemonType newOBJ) {
        for (int i = 0; i < pokemonTypeList.Count; i++) {
            if (pokemonTypeList[i].GetID() == id) pokemonTypeList[i] = newOBJ;
        }
    }



}
